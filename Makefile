#
# MIT License
# Copyright (c) 2017-2024 Nicola Worthington <nicolaw@tfb.net>
#
# https://gitlab.com/nicolaw/tiddlywiki
# https://nicolaw.uk
# https://nicolaw.uk/#TiddlyWiki
#

.PHONY: build push ami release
.DEFAULT_GOAL := build

TW_VERSION = 5.3.6
BASE_IMAGE = node:23.3-alpine3.19
REPOSITORY = nicolaw/tiddlywiki
USER       = node

IMAGE_TAGS = $(REPOSITORY):$(TW_VERSION) \
	           $(REPOSITORY):$(TW_VERSION)-$(subst /,,$(subst :,,$(BASE_IMAGE))) \
	           $(REPOSITORY):latest

MAKEFILE_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

DOCKERFILE = Dockerfile
DOCKER_BUILDX_PLATFORMS ?= 
ifneq ($(strip $(DOCKER_BUILDX_PLATFORMS)),)
  DOCKER_BUILD_CMD ?= docker buildx build --push --platform $(DOCKER_BUILDX_PLATFORMS)
else
  DOCKER_BUILD_CMD ?= docker build
endif

MAKEFILE = $(firstword $(MAKEFILE_LIST))
MAKEFILE_DIR = $(shell cd $(dir $(MAKEFILE)) && pwd)

AWS_REGIONS = $(shell aws ec2 describe-regions \
				--filter "Name=opt-in-status,Values=opt-in-not-required" \
				--output json --query 'Regions[].RegionName[]' \
				| tr -d '[[:space:]]')

build:
	DOCKER_BUILDKIT=1 $(DOCKER_BUILD_CMD) \
	  --no-cache \
	  --build-arg TW_VERSION=$(TW_VERSION) \
	  --build-arg BASE_IMAGE=$(BASE_IMAGE) \
	  --build-arg USER=$(USER) \
	  -f $(DOCKERFILE) \
	  $(addprefix -t ,$(IMAGE_TAGS)) \
	  $(MAKEFILE_DIR)

push:
	for t in $(IMAGE_TAGS) ; do docker $@ $$t ; done

release:
	$(MAKE) -f $(MAKEFILE) build DOCKER_BUILDX_PLATFORMS=linux/arm64,linux/amd64

ami:
	packer init -upgrade .
	packer build -var ami_regions='[$(AWS_REGIONS)]' .

